<?php
/**
 * JUSiteMap
 *
 * @version       1.x
 * @package       Joomla.Cli
 * @author        Denys D. Nosov (denys@joomla-ua.org)
 * @copyright (C) 2018 by Denys D. Nosov (https://joomla-ua.org)
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 *
 **/

/**
 * php /var/www/domains/site.org/cli/ju_sitemap.php --h=https --host=site.org
 */

const _JEXEC = 1;

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

if(file_exists(dirname(__DIR__) . '/defines.php'))
{
	require_once dirname(__DIR__) . '/defines.php';
}

if(!defined('_JDEFINES'))
{
	define('JPATH_BASE', dirname(__DIR__));
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';


class JUSitemap extends JApplicationCli
{
	public $helper = null;
	public $plugins = null;
	public $starttime = null;

	/**
	 * doExecute
	 *
	 * @since 1.0
	 */
	public function doExecute()
	{
		$this->starttime        = time();
		$_SERVER[ 'HTTP_HOST' ] = $this->input->get('host');
		$site                   = $this->input->get('h') . '://' . $this->input->get('host');

		$links = array();

		// apps

		// @ToDo: Edit count and apps
		$count = '100000';
		$apps  = array_merge(
			$this->menu($site, $links, 'mainmenu'), // add more menu types
			$this->seblod_items($site, $links, $count, '10', '232', 'news2'), // seblod apps
			$this->seblod_items($site, $links, $count, '21', '243', 'blogs'),
			$this->seblod_items($site, $links, $count, '22,23,26,24,25', '235', 'articles'),
			$this->seblod_items($site, $links, $count, '27', '245', 'caricature'),
			$this->seblod_items($site, $links, $count, '19', '241', 'gallery'),
			$this->seblod_items($site, $links, $count, '17', '240', 'interview'),
			$this->seblod_items($site, $links, $count, '30', '244', 'wzhistory'),
			$this->seblod_items($site, $links, $count, '18', '242', 'video')
		);
		// @ToDo: End edit

		// generation sitemap
		if(count($apps))
		{
			$parts      = 0;
			$i          = 0;
			$sitemapXML = '';

			$objDateTime = new DateTime('NOW');
			$tmp_date    = $objDateTime->format(DateTime::W3C);

			$disabled_urls = array(
				'manufacturer/view/',
				'orders/wishboxfrontadminorders/display',
				'sitemap?view=xml&amp;id=1',
			);

			foreach($apps as $app)
			{
				// sitemap structure
				if(!$i)
				{
					$parts++;

					$sitemapXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
					$sitemapXML .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\"\n";
					$sitemapXML .= "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n";
					$sitemapXML .= "xsi:schemaLocation=\"http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd\">\n";
					$sitemapXML .= "<!--Last update of sitemap " . $tmp_date . "-->\n";
				}

				$i++;
				if(!in_array($app[ 'url' ], $disabled_urls))
				{
					$sitemapXML .= "	<url>\n";
					$sitemapXML .= "		<loc>" . $app[ 'url' ] . "</loc>\n";

					if($app[ 'lastmod' ])
					{
						$idate      = new DateTime(JHtml::date($app[ 'lastmod' ], 'Y-m-d H:i:s'));
						$sitemapXML .= "		<lastmod>" . $idate->format(DateTime::W3C) . "</lastmod>\n";
					}

					$sitemapXML .= "		<changefreq>" . $app[ 'changefreq' ] . "</changefreq>\n";
					$sitemapXML .= "		<priority>0.5</priority>\n";
					$sitemapXML .= "	</url>\n";
				}

				if($i >= 20000)
				{
					$i = 0;

					$sitemapXML .= "</urlset>";

					$sitemapXML = trim(
						strtr(
							$sitemapXML,
							array(
								' % 2F' => ' / ',
								' % 3A' => ':',
								' % 3F' => ' ? ',
								' % 3D' => ' = ',
								' % 26' => ' & ',
								' % 27' => "'",
								'%22'   => '"',
								' % 3E' => ' > ',
								' % 3C' => ' < ',
								' % 23' => '#',
								'&'     => '&'
							)
						)
					);

					$fp = fopen(__DIR__ . '/../sitemaps/' . $parts . '.xml', 'w+');
					if(!fwrite($fp, $sitemapXML))
					{
						echo 'Error!';
					}

					fclose($fp);

					$sitemapXML = '';
				}
			}

			if($i)
			{
				$sitemapXML .= "</urlset>\n";

				$sitemapXML = trim(
					strtr(
						$sitemapXML,
						array(
							'%2F' => '/',
							'%3A' => ':',
							'%3F' => '?',
							'%3D' => '=',
							'%26' => '&',
							'%27' => "'",
							'%22' => '"',
							'%3E' => '>',
							'%3C' => '<',
							'%23' => '#',
							'&'   => '&'
						)
					)
				);

				$fp = fopen(__DIR__ . '/../sitemaps/' . $parts . '.xml', 'w+');
				if(!fwrite($fp, $sitemapXML))
				{
					echo 'Error!';
				}

				fclose($fp);
			}

			// sitemaps files
			$sitemapXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			$sitemapXML .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

			for($m = 1; $m <= $parts; $m++)
			{
				$sitemapXML .= "	<sitemap>\n";
				$sitemapXML .= "		<loc>" . $site . "/sitemaps/" . $m . ".xml</loc>\n";
				$sitemapXML .= "		<lastmod>" . $tmp_date . "</lastmod>\n";
				$sitemapXML .= "	</sitemap>\n";
			}

			$sitemapXML .= '</sitemapindex>';

			$sitemapXML = trim(
				strtr(
					$sitemapXML,
					array(
						'%2F' => '/',
						'%3A' => ':',
						'%3F' => '?',
						'%3D' => '=',
						'%26' => '&',
						'%27' => "'",
						'%22' => '"',
						'%3E' => '>',
						'%3C' => '<',
						'%23' => '#',
						'&'   => '&'
					)
				)
			);

			$fp = fopen(__DIR__ . '/../sitemap.xml', 'w+');
			if(!fwrite($fp, $sitemapXML))
			{
				echo 'Error!';
			}

			fclose($fp);
		}

		echo count($apps);
	}

	/**
	 * @param $site
	 * @param $links
	 * @param $menu
	 *
	 * @return mixed
	 *
	 * @since 1.0
	 */
	public function menu($site, $links, $menu)
	{
		$app   = JFactory::getApplication('site');
		$menus = $app->getMenu('site');
		$items = $menus->getItems('menutype', $menu);

		foreach($items as $item)
		{
			if(isset($item->link))
			{
				if($item->type == 'component')
				{
					if(!isset($links[ $item->link ]))
					{
						$links[ $item->link ] = array(
							'query'      => $item->link,
							'url'        => $site . str_replace(__DIR__, '', JRoute::_('index.php?Itemid=' . $item->id)),
							'lastmod'    => '',
							'changefreq' => 'daily',
						);
					}
				}
			}
		}

		return $links;
	}

	/**
	 * @param        $site
	 * @param        $links
	 * @param        $count
	 * @param        $catid
	 * @param        $menuitem
	 * @param        $type
	 * @param string $changefreq
	 *
	 * @return mixed
	 *
	 * @since 1.0
	 */
	public function seblod_items($site, $links, $count, $catid, $menuitem, $type, $changefreq = 'weekly')
	{
		$db  = JFactory::getDbo();
		$dbq = $db->getQuery(true);

		$dbq->select('tk.id, a.id, a.catid, a.modified, a.language');
		$dbq->from('#__cck_store_form_' . $type . ' as tk');
		$dbq->join('INNER', '#__content as a ON a.id = tk.id');
		$dbq->where('a.state = 1');

		if(is_array($catid))
		{
			$cat_arr = array();
			foreach($catid as $key => $curr)
			{
				if(intval($curr))
				{
					$cat_arr[ $key ] = intval($curr);
				}
			}
		}
		else
		{
			$cat_arr = array();
			if(intval($catid))
			{
				$cat_arr[] = intval($catid);
			}
		}

		if(is_array($cat_arr) && count($cat_arr))
		{
			$dbq->where('a.catid IN (' . implode(',', $cat_arr) . ')');
		}

		$dbq->order('a.created DESC');
		$db->setQuery($dbq, 0, $count);

		if($rows = $db->LoadObjectList())
		{
			foreach($rows as $row)
			{
				$query = $this->sebRouter('joomla_article', $type, $row->id, $menuitem);
				if(!isset($links[ $query ]))
				{
					$links[ $query ] = array(
						'query'      => $query,
						'url'        => $site . str_replace(__DIR__, '', JRoute::_($query)),
						'lastmod'    => ($row->modified ? $row->modified : ''),
						'changefreq' => $changefreq,
					);
				}
			}
		}

		return $links;
	}

	/**
	 * @param string $location
	 * @param null   $type
	 * @param null   $pk
	 * @param null   $itemId
	 * @param int    $sef
	 *
	 * @return mixed
	 *
	 * @since 1.0
	 */
	public function sebRouter($location = 'joomla_article', $type = null, $pk = null, $itemId = null, $sef = 0)
	{
		if($itemId > 0)
		{
			$target = JCckDatabase::loadResult('SELECT link FROM #__menu WHERE id = ' . (int) $itemId);
			if($target)
			{
				$vars = explode('&', $target);
				foreach($vars as $var)
				{
					$v = explode('=', $var);
					if($v[ 0 ] == 'search')
					{
						$target = $v[ 1 ];

						break;
					}
				}

				$vars = JCckDatabase::loadResult('SELECT options FROM #__cck_core_searchs WHERE name = "' . (string) $target . '"');
				if($vars)
				{
					$vars = new JRegistry($vars);
					$sef  = $vars->get('sef', JCck::getConfig_Param('sef', '2'));
				}
			}
		}

		require_once JPATH_SITE . '/plugins/cck_storage_location/' . $location . '/' . $location . '.php';

		return JCck::callFunc_Array('plgCCK_Storage_Location' . $location, 'getRoute', array(
			$pk,
			$sef,
			$itemId,
			array(
				'type' => $type
			)
		));
	}

}

JApplicationCli::getInstance('JUSitemap')->execute();